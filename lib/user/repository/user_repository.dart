import 'dart:async';

import 'package:avaliee_broker/user/repository/keycloak_oauth_client.dart';
import 'package:avaliee_broker/user/repository/user_base_repository.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meta/meta.dart';

class UserRepository implements UserBaseRepository {
  final KeycloakOAuthClient authClient;
  final FlutterSecureStorage storage;

  const UserRepository(
      {this.authClient = const KeycloakOAuthClient(),
      this.storage = const FlutterSecureStorage()});

  @override
  Future<String> signIn(
      {@required String username, @required String password}) async {
    var accessToken =
        await authClient.getAccessToken(username: username, password: password);
    await storage.write(key: 'token', value: accessToken);
    return accessToken;
  }

  @override
  Future<bool> isSignedIn() async {
    return await storage.read(key: 'token') != null;
  }

  @override
  Future<String> getAccessToken() async {
    return await storage.read(key: 'token');
  }

  @override
  Future<void> signOut() async {
    return await storage.delete(key: 'token');
  }
}
