import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

class KeycloakOAuthClient {
  const KeycloakOAuthClient();

  Future<String> getAccessToken(
      {@required String username, @required String password}) async {
    Map<String, String> authBody = {
      'client_id': 'avaliee-webapp',
      'username': username,
      'password': password,
      'grant_type': 'password'
    };

    // TODO handle auth error
    var oauthResponse = await http.post(
        'http://ec2-18-207-59-83.compute-1.amazonaws.com:8080/auth/realms/avaliee/protocol/openid-connect/token',
        body: authBody);

    if (oauthResponse.statusCode == 200) {
      var accessToken = json.decode(oauthResponse.body)['access_token'];

      return accessToken;
    } else {
      throw ("Credenciais inválidas");
    }
  }
}
