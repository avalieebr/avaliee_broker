import 'dart:async';
import 'dart:core';

import 'package:meta/meta.dart';

abstract class UserBaseRepository {
  Future<String> signIn({@required String username, @required String password});

  Future<bool> isSignedIn();

  Future<String> getAccessToken();

  Future<void> signOut();
}
