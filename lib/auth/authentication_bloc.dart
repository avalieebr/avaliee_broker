import 'dart:async';

import 'package:avaliee_broker/auth/authentication_event.dart';
import 'package:avaliee_broker/auth/authentication_state.dart';
import 'package:avaliee_broker/user/repository/user_base_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserBaseRepository userRepository;

  AuthenticationBloc({@required this.userRepository})
      : super(AuthenticationInitial());

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is AuthenticationStarted) {
      final bool isSignedIn = await userRepository.isSignedIn();

      if (isSignedIn) {
        final accessToken = await userRepository.getAccessToken();
        yield AuthenticationSuccess(accessToken);
      } else {
        yield AuthenticationFailure();
      }
    }

    if (event is AuthenticationLoggedIn) {
      yield AuthenticationInProgress();
      yield AuthenticationSuccess(event.token);
    }

    if (event is AuthenticationLoggedOut) {
      yield AuthenticationFailure();
      userRepository.signOut();
    }
  }
}
