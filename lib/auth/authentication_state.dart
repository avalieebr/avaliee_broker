import 'package:equatable/equatable.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();

  @override
  List<Object> get props => [];
}

class AuthenticationInitial extends AuthenticationState {}

class AuthenticationSuccess extends AuthenticationState {
  final String accessToken;

  const AuthenticationSuccess(this.accessToken);

  @override
  List<Object> get props => [accessToken];

  @override
  String toString() => 'AuthenticationSuccess { accessToken: $accessToken }';
}

class AuthenticationFailure extends AuthenticationState {}

class AuthenticationInProgress extends AuthenticationState {}
