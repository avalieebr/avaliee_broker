import 'package:avaliee_broker/auth/authentication_bloc.dart';
import 'package:avaliee_broker/auth/authentication_event.dart';
import 'package:avaliee_broker/auth/authentication_state.dart';
import 'package:avaliee_broker/leads/leads_bloc.dart';
import 'package:avaliee_broker/leads/leads_page.dart';
import 'package:avaliee_broker/leads/repository/leads_repository.dart';
import 'package:avaliee_broker/login/login_page.dart';
import 'package:avaliee_broker/user/repository/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:timeago/timeago.dart' as timeago;

void main() {
  timeago.setLocaleMessages(
      'pt_BR', timeago.PtBrMessages()); // Don't know if it should be here

  final userRepository = UserRepository();
  final leadsRepository = LeadsRepository();

  runApp(BlocProvider<AuthenticationBloc>(
    create: (context) {
      return AuthenticationBloc(userRepository: userRepository)
        ..add(AuthenticationStarted());
    },
    child:
        MyApp(userRepository: userRepository, leadsRepository: leadsRepository),
  ));
}

class MyApp extends StatelessWidget {
  final UserRepository userRepository;
  final LeadsRepository leadsRepository;

  MyApp(
      {Key key, @required this.userRepository, @required this.leadsRepository})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Avaliee Broker',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is AuthenticationSuccess) {
            return BlocProvider(
              create: (BuildContext context) => LeadsBloc(
                  leadsRepository: leadsRepository,
                  accessToken: state.accessToken),
              child: LeadsPage(title: 'Leads'),
            );
          } else {
            return LoginPage(userRepository: userRepository);
          }
        },
      ),
    );
  }
}
