import 'package:equatable/equatable.dart';

import 'lead.dart';

abstract class LeadsState extends Equatable {
  const LeadsState();

  @override
  List<Object> get props => [];

  List<Lead> get leads => [];
}

class LeadsLoadInProgress extends LeadsState {}

class LeadsLoadSuccess extends LeadsState {
  final List<Lead> leads;

  const LeadsLoadSuccess([this.leads = const []]);

  @override
  List<Object> get props => [leads];

  @override
  String toString() => 'LeadsLoadSuccess { leads: $leads }';
}

class LeadsLoadFailure extends LeadsState {}
