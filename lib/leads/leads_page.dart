import 'package:avaliee_broker/auth/authentication_bloc.dart';
import 'package:avaliee_broker/auth/authentication_event.dart';
import 'package:avaliee_broker/leads/lead_detail_page.dart';
import 'package:avaliee_broker/leads/leads_bloc.dart';
import 'package:avaliee_broker/leads/leads_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'lead.dart';

class LeadsPage extends StatefulWidget {
  LeadsPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LeadsPageState createState() => _LeadsPageState();
}

class _LeadsPageState extends State<LeadsPage> {
  @override
  Widget build(BuildContext context) {
    context.bloc<LeadsBloc>().add(LeadsEvent.LeadsLoad);
    return Scaffold(
      appBar: AppBar(title: Text(widget.title), actions: <Widget>[
        // action button
        IconButton(
          icon: Icon(Icons.exit_to_app),
          onPressed: () {
            BlocProvider.of<AuthenticationBloc>(context).add(
              AuthenticationLoggedOut(),
            );
          },
        )
      ]),
      body: Container(child: _buildMainWidget()),
    );
  }

  Widget _buildMainWidget() {
    return BlocBuilder<LeadsBloc, LeadsState>(
      builder: (context, state) {
        if (state is LeadsLoadInProgress) {
          return _buildLoadingWidget();
        } else if (state is LeadsLoadFailure) {
          return _buildErrorWidget();
        } else {
          return _buildListWidget((state as LeadsLoadSuccess).leads);
        }
      },
    );
  }

  Widget _buildLoadingWidget() {
    return Container(
      child: Center(
        child: Text('Carregando leads...'),
      ),
    );
  }

  Widget _buildErrorWidget() {
    return Container(
      child: Center(
        child: Text('Falha ao carregar leads'),
      ),
    );
  }

  Widget _buildListWidget(List<Lead> leads) {
    return ListView.builder(
      itemCount: leads.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(leads[index].customerName),
          subtitle:
              Text(timeago.format(leads[index].getDate(), locale: 'pt_BR')),
          onTap: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => LeadDetailPage(leads[index])));
          },
        );
      },
    );
  }
}
