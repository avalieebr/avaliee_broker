import 'dart:async';
import 'dart:convert';

import 'package:avaliee_broker/leads/repository/leads_base_repository.dart';
import 'package:http/http.dart' as http;

import '../lead.dart';

/// Do the communication with Leads API
class LeadsApiRepository implements LeadsBaseRepository {
  const LeadsApiRepository();

  @override
  Future<List<Lead>> loadLeads({String accessToken}) async {
    var leadsResponse = await http.get(
        'http://ec2-18-207-59-83.compute-1.amazonaws.com:8000/api/v1/leads',
        headers: {'Authorization': 'Bearer $accessToken'});

    return (json.decode(leadsResponse.body) as List)
        .map((i) => Lead.fromJson(i))
        .toList();
  }
}
