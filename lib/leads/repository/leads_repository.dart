import 'dart:async';
import 'dart:core';

import 'package:avaliee_broker/leads/lead.dart';
import 'package:avaliee_broker/leads/repository/leads_api_repository.dart';
import 'package:avaliee_broker/leads/repository/leads_base_repository.dart';

/// A class that glues together our local file storage (not yet) and api client
class LeadsRepository implements LeadsBaseRepository {
  final LeadsBaseRepository apiClient;

  const LeadsRepository({
    this.apiClient = const LeadsApiRepository(),
  });

  /// Load the leads from API
  @override
  Future<List<Lead>> loadLeads({String accessToken}) async {
    try {
      return await apiClient.loadLeads(accessToken: accessToken);
    } catch (e) {
      return [];
    }
  }
}
