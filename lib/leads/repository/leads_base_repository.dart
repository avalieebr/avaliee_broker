import 'dart:async';
import 'dart:core';

import '../lead.dart';

abstract class LeadsBaseRepository {
  Future<List<Lead>> loadLeads({String accessToken});
}
