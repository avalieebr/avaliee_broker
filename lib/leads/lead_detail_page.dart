import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

import 'lead.dart';

class LeadDetailPage extends StatelessWidget {
  final Lead lead;

  LeadDetailPage(this.lead);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(lead.customerName),
      ),
      body: _buildInformationWidget(),
    );
  }

  Widget _buildInformationWidget() {
    return Column(
      children: [
        ListTile(
          title: Text('E-mail'),
          subtitle: Text(lead.customerEmail),
        ),
        ListTile(
          title: Text('Telefone'),
          subtitle: Text(lead.customerPhone),
        ),
        ListTile(
          title: Text('Data de criação'),
          subtitle: Text(getFormattedDate(lead.createdAt)),
        ),
      ],
    );
  }

  String getFormattedDate(String serverDate) {
    initializeDateFormatting();
    return DateFormat(DateFormat.YEAR_MONTH_DAY, 'pt_br')
        .format(DateTime.now());
  }
}
