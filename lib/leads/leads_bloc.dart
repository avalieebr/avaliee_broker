import 'package:avaliee_broker/leads/leads_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import 'leads_state.dart';
import 'repository/leads_base_repository.dart';

enum LeadsEvent { LeadsLoad }

class LeadsBloc extends Bloc<LeadsEvent, LeadsState> {
  final LeadsBaseRepository leadsRepository;

  // Not sure if userRepository should be injected instead
  final String accessToken;

  LeadsBloc({@required this.leadsRepository, @required this.accessToken})
      : super(LeadsLoadInProgress());

  @override
  Stream<LeadsState> mapEventToState(LeadsEvent event) async* {
    switch (event) {
      case LeadsEvent.LeadsLoad:
        yield* _mapLeadsLoadedToState();
        break;
    }
  }

  Stream<LeadsState> _mapLeadsLoadedToState() async* {
    try {
      final leads =
          await this.leadsRepository.loadLeads(accessToken: this.accessToken);
      leads.sort((a, b) => b.createdAt.compareTo(a.createdAt));
      yield LeadsLoadSuccess(leads);
    } catch (_) {
      yield LeadsLoadFailure();
    }
  }
}
