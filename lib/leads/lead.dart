import 'package:equatable/equatable.dart';

class Lead extends Equatable {
  final String id;
  final String customerName;
  final String customerEmail;
  final String customerPhone;
  final String createdAt;

  Lead(this.id, this.customerName, this.customerEmail, this.customerPhone,
      this.createdAt);

  @override
  List<Object> get props => [id, customerName, customerEmail, customerPhone];

  static Lead fromJson(Map<String, Object> json) {
    return Lead(
        json['id'] as String,
        json['customerName'] as String,
        json['customerEmail'] as String,
        json['customerPhone'] as String,
        json['createdAt'] as String);
  }

  @override
  String toString() {
    return 'Lead { customerName: $customerName, customerEmail: $customerEmail,'
        'customerPhone: $customerPhone, id: $id }';
  }

  DateTime getDate() {
    return DateTime.parse(createdAt);
  }
}
