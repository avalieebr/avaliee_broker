import 'dart:async';

import 'package:avaliee_broker/auth/authentication_bloc.dart';
import 'package:avaliee_broker/auth/authentication_event.dart';
import 'package:avaliee_broker/login/login_event.dart';
import 'package:avaliee_broker/login/login_state.dart';
import 'package:avaliee_broker/user/repository/user_base_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserBaseRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  }) : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginInProgress();

      try {
        final token = await userRepository.signIn(
          username: event.username,
          password: event.password,
        );

        authenticationBloc.add(AuthenticationLoggedIn(token: token));
        yield LoginInitial();
      } catch (error) {
        print(error.toString());
        yield LoginFailure(error: error.toString());
      }
    }
  }
}
