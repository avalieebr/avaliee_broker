import 'package:avaliee_broker/auth/authentication_bloc.dart';
import 'package:avaliee_broker/login/login_bloc.dart';
import 'package:avaliee_broker/login/login_form.dart';
import 'package:avaliee_broker/user/repository/user_base_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatelessWidget {
  final UserBaseRepository userRepository;

  LoginPage({Key key, @required this.userRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avaliee Broker'),
      ),
      body: BlocProvider(
        create: (context) {
          return LoginBloc(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
            userRepository: userRepository,
          );
        },
        child: LoginForm(),
      ),
    );
  }
}
