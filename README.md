# avaliee_broker

A Flutter project for the Avaliee broker user.

This application allows an authenticated user to list and see details about the leads created in 
the Avaliee search engine.

## Getting Started

As this is a Flutter project you may find all information
on [online documentation](https://flutter.dev/docs/get-started/install).

This application requires Flutter version 1.17+.

### Setup

- Clone this repository
```bash
$ git clone git@bitbucket.org:avalieebr/avaliee_broker.git
```

### Building and running

- Debug mode
```bash
$ flutter run -t lib/main.dart
```

- Release
```bash
$ flutter run --release
```

## System architecture overview

![](./docs/arch-overview.png)

## Application architecture overview

This application is implemented following [BLoC pattern](https://pub.dev/packages/flutter_bloc)
which helps to separate presentation code from business logic code.

### Screens

- Splash screen
- Login screen
- Lead listing screen
- Lead details screen

### Usage demo

[Simple navigation demo video](./docs/demo.mp4)
